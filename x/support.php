<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>participic</title>
	<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>


<div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img id="logo" height="30" src="ppic2.png"/></a>
			<div class="visible-xs" style="margin-left: .8em; margin-top:-1em; ">
				<br/><br/>
				<ul class="  nav navbar-nav">
					<li><a href="#"><i>every</i> picture, <i>every</i> camera, <i>every</i> event</a></li>
					<li><a target="_blank" href="http://twitter.com/participic">@particpic</a></li>
				</ul>
			</div>
		</div>
		<div class="pull-right hidden-xs">
			<ul class="nav navbar-nav">
				<li><a href="#"><i>every</i> picture, <i>every</i> camera, <i>every</i> event</a></li>
				<li><a target="_blank" href="http://twitter.com/participic">@particpic</a></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</div>

<div class="container">

	<div class="row hidden-xs" style="margin-top:2em">
	</div>
	<div class="row">
		<div class="col-md-8 col-lg-8" style="margin-top:1em">
			<p class="center-block">
				<img class="img-responsive"  src="images/logo4.png"/>
			</p>

			<p class="center-block">

				<h2>support</h2>
			</p>
			<p>
				If you have any questions about participic, or need support, please email support@participic.com, or 
				give us a call at 919-827-4724.
			</p>

		</div>
		<div class="col-md-4 col-lg-4">

			<img class="center-block" src="images/ppicimage.png"/>
			<br/><br/>
		</div>
	</div>

	<div class="row">
		<br/>
	</div>
	<div class="row">
		<br/>
	</div>
	<div class="row">
		<br/>
	</div>

</div>
<!-- /.container -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>
