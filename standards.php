<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Content Standards</title>
</head>

<?php
$updatedDate = "July 10, 2014";
$companyName = "beElite, LLC";
$privacyPolicyFull= "<a href='http://participic.com/docs/privacy.php'>Privacy Policy</a>";
$domainName = "http://participic.com";
$domainName = "participic.com";
$supportEmailAddress = "support@participic.com";
$companyAddress = "310 S Harrington St, Raleigh, NC 27601";
$standardsLink = "<a href='http://participic.com/docs/standards.php'>Content Standards</a>";
?>


<body>

<p>
    <strong>Content Standards</strong>
</p>
<p>
    Last Modified: <?=$updatedDate;?>
</p>


<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Violence and Threats</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">Safety is a top priority for us. We remove content and may escalate to law enforcement when we perceive a genuine risk of physical harm, or a direct threat to public safety. You may not credibly threaten others, or organize acts of real-world violence. Organizations with a record of terrorist or violent criminal activity are not allowed to maintain a presence on our site. We also prohibit promoting, planning or celebrating any of your actions if they have, or could, result in financial or other harm to others, including theft and vandalism.</span></div></div>
<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Self-Harm</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">We takes threats of self-harm very seriously. We remove any promotion or encouragement of self-mutilation, eating disorders or hard drug abuse. </span></div></div>
<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Bullying and Harassment</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">We do not tolerate bullying or harassment. We allow users to speak freely on matters and people of public interest, but take action on all reports of abusive behavior directed at private individuals. Repeatedly targeting other users with unwanted friend requests or messages is a form of harassment.</span></div></div>
<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Hate Speech</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">We do not permit hate speech, but we distinguish between serious and humorous speech. While we encourage you to challenge ideas, institutions, events, and practices, we do not permit individuals or groups to attack others based on their race, ethnicity, national origin, religion, sex, gender, sexual orientation, disability or medical condition</span></div></div>
<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Graphic Content</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">We are a place where people turn to share their experiences and raise awareness about issues important to them. Sometimes, those experiences and issues involve graphic content that is of public interest or concern, such as human rights abuses or acts of terrorism. In many instances, when people share this type of content, it is to condemn it. However, graphic images shared for sadistic effect or to celebrate or glorify violence have no place on our site.<br><br> When people share any content, we expect that they will share in a responsible manner. That includes choosing carefully the audience for the content. For graphic videos, people should warn their audience about the nature of the content in the video so that their audience can make an informed choice about whether to watch it. </span></div></div>
<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Nudity and Pornography</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">We have a strict policy against the sharing of pornographic content and any explicitly sexual content where a minor is involved. We also impose limitations on the display of nudity. We aspire to respect people’s right to share content of personal importance, whether those are photos of a sculpture like Michelangelo's David or family photos of a child breastfeeding. </span></div></div>
<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Identity and Privacy</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">On participic, people connect using their real names and identities. We ask that you refrain from publishing the personal information of others without their consent. Claiming to be another person, creating a false presence for an organization, or creating multiple accounts undermines community and violates our terms.</span></div></div>
<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Intellectual Property</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">Before sharing content on our service, please be sure you have the right to do so. We ask that you respect copyrights, trademarks, and other legal rights.</span></div></div>
<h4 class="marketingBodyTitle lfloat _ohe marketingBodyTitleMedium">Phishing and Spam</h4>
<div class="marketingBodyCopy communityStandardsText rfloat _ohf fsl"><span class="fsm">We take the safety of our members seriously and work to prevent attempts to compromise their privacy or security. We also ask that you respect our members by not contacting them for commercial purposes without their consent. </span></div></div>
</body>
</html>
